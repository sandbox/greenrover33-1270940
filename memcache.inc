<?php
/** Implementation of cache.inc with memcache logic included **/

require_once 'dmemcache.inc';
require_once 'cache_db.inc';

class educa_memcache {
 protected static $memcache_bins = null;
 private static $instance = null;
 
 public static function getInstance() {
   if(self::$instance == null) {
     #file_put_contents('/tmp/debug.cache', "create instance\n", FILE_APPEND);
     self::$instance = new educa_memcache();
   }

   return self::$instance;
 }

 function __construct() {
  $this->memcache_bins = dmemcache_get('existing', 'memcache');
  if (!is_array($this->memcache_bins)) {
   $this->memcache_bins = array();
  }
  #file_put_contents('/tmp/debug.cache', "cache init: ".(!empty($this->memcache_bins['cache']['variables'])?'1':'0')."\n", FILE_APPEND);
  #file_put_contents('/tmp/debug.cache', "cache init: ".@implode(', ', array_keys($this->memcache_bins['cache']))."\n", FILE_APPEND);

  register_shutdown_function(array($this, 'memcache_shutdown'));

  return true;
 }
 
 function memcache_shutdown() {
  #file_put_contents('/tmp/debug.cache', "cache shutdown: ".(!empty($this->memcache_bins['cache']['variables'])?'1':'0')."\n", FILE_APPEND);
  dmemcache_set('existing', $this->memcache_bins, 0, 'memcache');

  #file_put_contents('/tmp/debug.cache', "shutdown: ".@implode(', ', array_keys($this->memcache_bins['cache']))."\n", FILE_APPEND);
 }

/**
 * Return data from the persistent cache.
 *
 * Data may be stored as either plain text or as serialized data.
 * cache_get() will automatically return unserialized objects and arrays.
 *
 * @param $cid
 *   The cache ID of the data to retrieve.
 * @param $table
 *   The table $table to store the data in. Valid core values are 'cache_filter',
 *   'cache_menu', 'cache_page', or 'cache' for the default cache.
 */
 public function cache_get($cid, $table = 'cache') {  # cache_form
  if (empty($this->memcache_bins[$table][$cid])) {
   #return false;
  }

  if ($table=='cache_form') {
   $data = dmemcache_get($cid, $table);
   return $data;
  }

  // Retrieve the item from the cache.
  return dmemcache_get($cid, $table);
 }

/**
 * Store data in memcache.
 *
 * @param $cid
 *   The cache ID of the data to store.
 * @param $data
 *   The data to store in the cache. Complex data types will be automatically
 *   serialized before insertion. Strings will be stored as plain text and
 *   not serialized.
 * @param $table
 *   The table $table to store the data in. Valid core values are 'cache_filter',
 *   'cache_menu', 'cache_page', or 'cache'.
 * @param $expire
 *   One of the following values:
 *   - CACHE_PERMANENT: Indicates that the item should never be removed unless
 *     explicitly told to using cache_clear_all() with a cache ID.
 *   - CACHE_TEMPORARY: Indicates that the item should be removed at the next
 *     general cache wipe.
 *   - A Unix timestamp: Indicates that the item should be kept at least until
 *     the given time, after which it behaves like CACHE_TEMPORARY.
 * @param $headers
 *   A string containing HTTP header information for cached pages.
 */
 public function cache_set($cid, $data, $table = 'cache', $expire = CACHE_PERMANENT, $headers = NULL) {
    $created = $_SERVER['REQUEST_TIME'];

    // Create new cache object.
    $cache = new stdClass;
    $cache->cid = $cid;
    $cache->data = is_object($data) ? memcache_clone($data) : $data;
    $cache->created = $created;
    $cache->headers = $headers;
    if ($expire == CACHE_TEMPORARY) {
      // Convert CACHE_TEMPORARY (-1) into something that will live in memcache
      // until the next flush.
      $cache->expire = $_SERVER['REQUEST_TIME'] + 2591999;
    }
    // Expire time is in seconds if less than 30 days, otherwise is a timestamp.
    else if ($expire != CACHE_PERMANENT && $expire < 2592000) {
      // Expire is expressed in seconds, convert to the proper future timestamp
      // as expected in dmemcache_get().
      $cache->expire = $_SERVER['REQUEST_TIME'] + $expire;
    }
    else {
      $cache->expire = $expire;
    }

    $cache_lifetime = variable_get('cache_lifetime', 0);
    if (!empty($cache_lifetime)) {
     $cache_lifetime+=time();

     $cache->expire = min($cache->expire, $cache_lifetime);
    }

    $this->memcache_bins[$table][$cid] = true;
    #file_put_contents('/tmp/debug.cache', "cache_set: $table # $cid # ".(!empty($this->memcache_bins['cache']['variables'])?'1':'0')."\n", FILE_APPEND);
    #file_put_contents('/tmp/debug.cache', "cache_set: ".implode(', ', array_keys($this->memcache_bins['cache']))."\n", FILE_APPEND);

    // We manually track the expire time in $cache->expire.  When the object
    // expires, we only allow one request to rebuild it to avoid cache stampedes.
    // Other requests for the expired object while it is still being rebuilt get
    // the expired object.
    dmemcache_set($cid, $cache, $cache->expire, $table);
  }

/**
 *
 * Expire data from the cache. If called without arguments, expirable
 * entries will be cleared from the cache_page and cache_block tables.
 *
 * Memcache logic is simpler than the core cache because memcache doesn't have
 * a minimum cache lifetime consideration (it handles it internally), and
 * doesn't support wildcards.  Wildcard flushes result in the entire table
 * being flushed.
 *
 * @param $cid
 *   If set, the cache ID to delete. Otherwise, all cache entries that can
 *   expire are deleted.
 *
 * @param $table
 *   If set, the table $table to delete from. Mandatory
 *   argument if $cid is set.
 *
 * @param $wildcard
 *   If set to TRUE, the $cid is treated as a substring
 *   to match rather than a complete ID. The match is a right hand
 *   match. If '*' is given as $cid, the table $table will be emptied.
 */
  public function cache_clear_all($cid = NULL, $table = NULL, $wildcard = FALSE) {
   #file_put_contents('/tmp/debug.cache', "cache_clear_all: $table # $cid # ".(!empty($this->memcache_bins['cache']['variables'])?'1':'0')."\n", FILE_APPEND);

   // Default behavior for when cache_clear_all() is called without parameters
   // is to clear all of the expirable entries in the block and page caches.
   if (!isset($table)) {
    dmemcache_object('flush', true);
    unset($this->memcache_bins);
    $this->memcache_bins = array();

    return;
   }

   if ($cid=='*') {
    $wildcard = true;
   }


   if ($wildcard==true) {
    $to_delete = array_keys($this->memcache_bins[$table]);

   } else {
    $l = strlen($cid);
    $to_delete = array();
    if (!empty($this->memcache_bins[$table])) {
     foreach($this->memcache_bins[$table] AS $key=>$foo) {
      if (substr($key,0,$l)==$cid) {
       $to_delete[] = $key;
      }
     }
    }
   }

   #if ($table=='cache') {
   # file_put_contents('/tmp/debug.cache', "cache_clear_all: ".implode(', ', $this->memcache_bins['cache'])."\n", FILE_APPEND);
   # file_put_contents('/tmp/debug.cache', "cache_clear_all: ".implode(', ', $to_delete)."\n", FILE_APPEND);
   #}

   foreach($to_delete AS $key) {
    dmemcache_delete($key, $table);
    unset($this->memcache_bins[$table][$key]);
    #file_put_contents('/tmp/debug.cache', "dmemcache_delete: $table # $key # ".(!empty($this->memcache_bins['cache']['variables'])?'1':'0')."\n", FILE_APPEND);
   }
  }
}

function cache_get($cid, $table = 'cache') {
  if ($table=='cache_form') {
   return db_cache_get($cid, $table);
  }

  $cache = educa_memcache::getInstance();
  return $cache->cache_get($cid, $table);
}

function cache_set($cid, $data, $table = 'cache', $expire = CACHE_PERMANENT, $headers = NULL) { 
  if ($table=='cache_form') {
   return db_cache_set($cid, $data, $table, $expire, $headers);
  }

  $cache = educa_memcache::getInstance();
  return $cache->cache_set($cid, $data, $table, $expire, $headers);
}

function cache_clear_all($cid = NULL, $table = NULL, $wildcard = FALSE) {
  if ($table=='cache_form') {
   return db_cache_clear_all($cid, $table, $wildcard);
  }

  $cache = educa_memcache::getInstance();
  return $cache->cache_clear_all($cid, $table, $wildcard);
}

/**
 * Provide a substitute clone() function for PHP4. This is a copy of drupal_clone
 * because common.inc isn't included early enough in the bootstrap process to
 * be able to depend on drupal_clone.
 */
function memcache_clone($object) {
  return version_compare(phpversion(), '5.0') < 0 ? $object : clone($object);
}

function memcache_debug_log($type, $table, $key, $value) {
 $cache_key = '';
 $time = strftime('%Y-%m-%d_%H-%M-%S');
 $length = strlen($value);
 if (!empty($length)) {
  $cache_key = $time.'_'.uniqid(mt_rand(), true);
  file_put_contents('/tmp/drupal_memcache/'.$cache_key, print_r($value, true));
 }

 file_put_contents('/tmp/drupal_memcache.log',
                   session_id()."\t".$time."\t".$table."\t".$key."\t".$length."\t".$cache_key."\n",
                   FILE_APPEND
                  );
}